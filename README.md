# Apache Spark and Hive useful functions #

Another night o preparation for [Hortonworks Spark certification](https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/).
This time I wanted to try some approaches and functions of Spark and Apache Hive. Let's go step by step.

## 1. Spark and countByValue function ##

Let's have following RDD of values:

    var rddVal = sc.parallelize(Array(1,2,2,3,4,4,4,5,5,5,6));

our task is to create new RDD where key will be the unique item value from rddVal and value is going to be the **number of rddVal item occurences**.
[countByValue](https://spark.apache.org/docs/1.5.1/api/java/org/apache/spark/rdd/RDD.html#countByValue(scala.math.Ordering))
is excellent tool for that.

    %spark

    var rddVal = sc.parallelize(Array(1,2,2,3,4,4,4,5,5,5,6));

    val countedRDD = sc.parallelize(rddVal.countByValue().toSeq);

    countedRDD.collect();
	
zeppelin output:

    rddVal: org.apache.spark.rdd.RDD[Int] = ParallelCollectionRDD[3] at parallelize at <console>:29
    countedRDD: org.apache.spark.rdd.RDD[(Int, Long)] = ParallelCollectionRDD[7] at parallelize at <console>:31
    res2: Array[(Int, Long)] = Array((5,3), (1,1), (6,1), (2,2), (3,1), (4,3))


## 2. Spark and countByKey function ##

Sometimes we're having (key,value) RDD and we want to count number of occurences of all keys.
[countByKey](https://spark.apache.org/docs/2.2.0/rdd-programming-guide.html) action function is an excellent tool for that!

    %spark
    var rddKeyValues = sc.parallelize(Array(("A", 99), ("A",88), ("B",22), ("C",33)));

    val countedKeys = sc.parallelize(rddKeyValues.countByKey().toSeq);

    countedKeys.collect();

zeppelin output:

    rddKeyValues: org.apache.spark.rdd.RDD[(String, Int)] = ParallelCollectionRDD[10] at parallelize at <console>:29
    countedKeys: org.apache.spark.rdd.RDD[(String, Long)] = ParallelCollectionRDD[13] at parallelize at <console>:31
    res4: Array[(String, Long)] = Array((B,1), (A,2), (C,1))

## 3. Spark and keyBy function ##

If you're having an RDD of values and you want to apply a function to every element in there 
and **result of the function should be the new RDD's item key** then [keyBy](https://spark.apache.org/docs/1.4.0/api/java/org/apache/spark/rdd/RDD.html#keyBy(scala.Function1)) function is your friend.

    %spark

    def multiply(num: Int):Int={
        return num*num;
    }

    val inputRDD = sc.parallelize(Array(1,2,3,4,5,6));

    val resRDD = inputRDD.keyBy(multiply);

    resRDD.collect();

zeppelin output:

    multiply: (num: Int)Int
    inputRDD: org.apache.spark.rdd.RDD[Int] = ParallelCollectionRDD[14] at parallelize at <console>:29
    resRDD: org.apache.spark.rdd.RDD[(Int, Int)] = MapPartitionsRDD[15] at keyBy at <console>:33
    res5: Array[(Int, Int)] = Array((1,1), (4,2), (9,3), (16,4), (25,5), (36,6))


## 4. Apache Hive and switch to another database ##

Something obvious, but let's show howto do that!

    %spark
    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);

    hc.sql("USE xademo");
    hc.sql("SHOW TABLES").show();

zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@235d1d35
    res11: org.apache.spark.sql.DataFrame = [result: string]
    +-------------------+-----------+
    |          tableName|isTemporary|
    +-------------------+-----------+
    |call_detail_records|      false|
    |   customer_details|      false|
    |             genres|      false|
    |            justone|      false|
    |            mytable|      false|
    |      mytablexademo|      false|
    |   recharge_details|      false|
    |          workersxa|      false|
    +-------------------+-----------+

## 5. Dataframe partitioning ##

Sometimes you're asked to save dataframe into HDFS in some particular format and you're forced to use dynamic partitioning.
Let's show howto do that:

Input file workers.txt:

    [root@sandbox ~]# cat workers.txt 
    1,Jerry,man,USA
    2,Cathy,female,GBR
    3,Teresa,female,GBR
    4,Rut,female,USA
    5,Roasie,female,AUS
    6,Garry,man,GBR
    7,Adam,man,GER
    8,John,man,GBR
    9,Jerremy,man,AUS
    10,Angela,female,AUS
    11,Ivanka,female,USA
    12,Melania,female,USA

Spark code:

    %spark

    // Dynamic partitioning when saving from Dataframe to HDFS

    case class worker(id: Int, name: String, sex: String, country: String);

    val fileRDD = sc.textFile("/tests/workers.txt");

    val workerDF = fileRDD.map(line=>new worker(line.split(",")(0).toInt, 
                                            line.split(",")(1), 
                                            line.split(",")(2), 
                                            line.split(",")(3))).toDF();
											
    // save dataframe also into Hive for further use                                        
    workerDF.saveAsTable("tableWorkers");

    workerDF.write
            .mode("overwrite")
            .partitionBy("country")
            .json("/tests/partition/result");


zeppelin output:

    defined class worker
    fileRDD: org.apache.spark.rdd.RDD[String] = /tests/workers.txt MapPartitionsRDD[289] at textFile at <console>:114
    workerDF: org.apache.spark.sql.DataFrame = [id: int, name: string, sex: string, country: string]
    warning: there were 1 deprecation warning(s); re-run with -deprecation for details

more interesting is HDFS result:

    [root@sandbox ~]# hdfs dfs -ls /tests/partition/result
    Found 5 items
    -rw-r--r--   1 zeppelin hdfs          0 2018-08-13 23:35 /tests/partition/result/_SUCCESS
    drwxr-xr-x   - zeppelin hdfs          0 2018-08-13 23:35 /tests/partition/result/country=AUS
    drwxr-xr-x   - zeppelin hdfs          0 2018-08-13 23:35 /tests/partition/result/country=GBR
    drwxr-xr-x   - zeppelin hdfs          0 2018-08-13 23:35 /tests/partition/result/country=GER
    drwxr-xr-x   - zeppelin hdfs          0 2018-08-13 23:35 /tests/partition/result/country=USA
    [root@sandbox ~]# 

Spark created folder for every partition where every partition contains grouped data (for example):

    [root@sandbox ~]# hdfs dfs -cat /tests/partition/result/country=USA/part-r-00000-9adc651a-1260-466d-ba37-720a0395d450
    {"id":1,"name":"Jerry","sex":"man"}
    {"id":4,"name":"Rut","sex":"female"}

## 6. Reading partitioned HDFS data back to dataframe ##

Well, of course it works!

    %spark
    val backDFJson = sqlContext.read.json("/tests/partition/result");

    backDFJson.show();

zeppelin output:

    backDFJson: org.apache.spark.sql.DataFrame = [id: bigint, name: string, sex: string, country: string]
    +---+-------+------+-------+
    | id|   name|   sex|country|
    +---+-------+------+-------+
    |  1|  Jerry|   man|    USA|
    |  4|    Rut|female|    USA|
    | 11| Ivanka|female|    USA|
    | 12|Melania|female|    USA|
    |  5| Roasie|female|    AUS|
    |  9|Jerremy|   man|    AUS|
    | 10| Angela|female|    AUS|
    |  7|   Adam|   man|    GER|
    |  2|  Cathy|female|    GBR|
    |  3| Teresa|female|    GBR|
    |  6|  Garry|   man|    GBR|
    |  8|   John|   man|    GBR|
    +---+-------+------+-------+

## 7. Apache Hive and dynamic partitioning of ORC table ##

Apache Hive supports two kind of partitioning:

* Static partitioning
* Dynamic partitioning

For further informations I can recommend following [blog](https://resources.zaloni.com/blog/partitioning-in-hive). Basic difference is that
when you're saving data into statically partitioned table then you have to name the partition column with value distinguishing the partition. 
I case of dynamic partitioning, partition is created if not existing. No value needed, just partition column.

**Test task**: 
Let's create ORC table of workers dynamically partitioned by country. And save data into it from previously created table "tableWorkers".

    %spark

    // dynamic partitioning on Hive Table...
    import org.apache.spark.sql.hive.HiveContext;

    var hc = new HiveContext(sc);

    hc.sql(" DROP TABLE IF EXISTS WorkersPartitioned ");
    hc.sql(" CREATE TABLE WorkersPartitioned(id INT, name String, sex String) "+
       " PARTITIONED BY (country STRING) "+
       " STORED AS ORC "
    );

    hc.sql(" SET set hive.exec.dynamic.partition=true ");
    hc.sql(" SET hive.exec.dynamic.partition.mode=nonstric ");

    hc.sql(" INSERT OVERWRITE TABLE WorkersPartitioned PARTITION(country) SELECT id, name, sex, country FROM tableWorkers ");

    hc.sql(" SELECT * FROM WorkersPartitioned ").show();

Notice the code "PARTITION(country)", we didn't need to enter exact country, that's dynamic partitioning.

Zeppelin output:

    res165: org.apache.spark.sql.DataFrame = [key: string, value: string]
    res166: org.apache.spark.sql.DataFrame = []
    +---+-------+------+-------+
    | id|   name|   sex|country|
    +---+-------+------+-------+
    |  5| Roasie|female|    AUS|
    |  9|Jerremy|   man|    AUS|
    | 10| Angela|female|    AUS|
    |  2|  Cathy|female|    GBR|
    |  3| Teresa|female|    GBR|
    |  6|  Garry|   man|    GBR|
    |  8|   John|   man|    GBR|
    |  7|   Adam|   man|    GER|
    |  1|  Jerry|   man|    USA|
    |  4|    Rut|female|    USA|
    | 11| Ivanka|female|    USA|
    | 12|Melania|female|    USA|
    +---+-------+------+-------+

## 8. Apache Hive and describing of partioning at table ##

    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);

    hc.sql("show partitions WorkersPartitioned").show();

Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@2a117cf4
    +-----------+
    |     result|
    +-----------+
    |country=AUS|
    |country=GBR|
    |country=GER|
    |country=USA|
    +-----------+

I hope you found my tests useful. I attached whole Zeppelin notebook for you if you need to do more tests.

regards

Tomas